# Building a mothur pipleine #

First things first you are going to need:

	1) A question - What is the structure of the metagenomic data that I am studying?
	2) A group of fasta files (of course)
	3) A good text editor (which you do)
	4) mothur and its dependencies
	5) conda, docker, and snakemake
	
### What is this repository for? ###

* This is going to be a tutorial on building a mothur pipleine. Ideally, by the end we will be able to construct a pipeline that will allow for one input line in the command line (mothur --fastadb --processors)


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact